import React, { useEffect } from 'react';

export default function Carousel(props) {
    useEffect(() => {
        console.log('render carousel');
        window.jQuery = require('../public/vendor/jquery/jquery.min.js');
        window.Carousel = require('../public/vendor/owl-carousel/owl.carousel.js');

        window.jQuery('#carousel').owlCarousel({
            margin: 15,
            dots: props.showDots,
            nav: props.showNavs && !props.customNavs,
            navText: props.customNavs && [`<img src='${props.customNavs.previous}'>`, `<img src='${props.customNavs.next}'>`],
            responsive: {
                0: {
                    items: 1
                },
                768: {
                    items: 2
                },
                1280: {
                    items: 3
                },
                1600: {
                    items: 4
                },
                2560: {
                    items: 6
                }
            }
        });
        window.jQuery('.owl-dots').addClass('mt-0');

        //handle custom navigations
        props.customNavs && (
            //go to previous slide
            window.jQuery(`#${props.customNavs.previous}`).on('click', function () {
                window.jQuery('#carousel').trigger('prev.owl.carousel');
            }),

            //go to next slide
            window.jQuery(`#${props.customNavs.next}`).on('click', function () {
                window.jQuery('#carousel').trigger('next.owl.carousel');
            })
        )
        //END handle custom navigations
    }, [props.children]);


    return (

        <div className="owl-carousel owl-theme mb-0 owl-loaded owl-drag" id="carousel">
            {props.children}
        </div>
    );
};