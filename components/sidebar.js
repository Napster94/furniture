import { useEffect, useState } from 'react';

const SideNav = (props) => {
    /* 
        theme = dark || light
        position = left || right
        activeColor = '#ffcc00'
        activeLink = {activeLink} //current page's slug
        onNavChange = {(url) => { setActiveLink(url) }} //whatever function to handle nav changes
        links = {[
          {
            text: 'Link 1', //text in nav link
            icon: "/img/map.svg", //nav link icon
            externalLink = true || false //if item does not affect page navigation (NB: link will be opened in new tab)
            activeIcon: "/img/map-active.svg", nav link icon when active
            url: '#', nav link url
            className: '', any extra classes to pass to the link
            slug: 'link-1' //slug for the nav link
          }
        ]}
    */

    console.log(props);

    const [openNav, setOpenNav] = useState(false);
    const [showNavSpan, setShowNavSpan] = useState(false);

    const toggleNav = (_val) => {
        setOpenNav(_val);
    }

    async function _handleNavClick(_url) {

    }

    useEffect(() => {
        console.log('toggling nav...', openNav);

        (openNav ?
            (
                setShowNavSpan(true)
            ) : (
                setShowNavSpan(false)
            )
        )
    }, [openNav]);


    return (
        <nav className={`${props.mobile ? 'd-none' : ''}`} style={{ padding: 0, margin: 0 }} onMouseEnter={() => { !props.static && toggleNav(true) }} onMouseLeave={() => { !props.static && toggleNav(false) }}>
            <style global jsx>
                {`
                    #main {
                        width: calc(100% - 60px);
                        margin-left: ${props.position === 'left' ? '60px !important' : '0'};
                        margin-right: ${props.position === 'right' ? '60px' : '0'};
                    }
                    #sidebar {
                        width: ${showNavSpan ? '180px' : '60px'};
                        left: ${props.position === 'left' ? '0px !important' : 'inherit'};
                        right: ${props.position === 'right' ? '0px !important' : 'inherit'};
                        background-color: ${props.bgColor} !important;
                    }
                `}
            </style>
            <div id='sidebar' className={`sidebar ${props.theme === 'dark' ? 'bg-dark' : 'bg-light'}`}>
                <div className="sidebar-sticky">
                    <ul className="nav flex-column" style={{ marginTop: '3em' }}>
                        {props.links.map((_lnk, i) => {
                            return (
                                !_lnk.externalLink ? (
                                    <li className={`nav-item ${openNav ? 'px-3 py-2' : 'p-3'} my-2 ${!openNav && 'row mx-0'} ${props.activeLink === _lnk.slug ? 'active' : ''}`} style={{ cursor: 'pointer' }} key={i}>
                                        <a className={`text-decoration-none mx-auto`} href={_lnk.url} onClick={() => { props.onNavChange(_lnk.slug) }}>
                                            {_lnk.icon &&
                                                <img className={`px-0`} src={`${props.activeLink === _lnk.slug ? _lnk.activeIcon : _lnk.icon}`} />
                                            }
                                            {showNavSpan &&
                                                <span className={`pl-3 pr-0 mb-0 my-auto ${props.activeLink === _lnk.slug ? 'text-primary' : props.theme === "dark" ? 'text-light' : 'text-dark'} ${props.activeLink === _lnk.slug ? 'font-weight-bold' : 'font-weight-normal'}`} style={{ color: props.activeLink === _lnk.slug ? props.activeColor : 'inherit' }} > {_lnk.text} </span>
                                            }
                                        </a>
                                    </li>
                                ) :
                                    (
                                        <li className={`nav-item p-3 my-2`} style={{ cursor: 'pointer' }} key={i}>
                                            <a className="text-decoration-none row mx-auto" href={_lnk.url} onClick={() => { window.open(_lnk.url, '_blank'); }}>
                                                {showNavSpan &&
                                                    <span className={`col-12 px-0 my-auto text-center ${props.theme === "dark" ? 'text-light' : 'text-dark'} ${props.activeLink === _lnk.slug ? 'font-weight-bold' : 'font-weight-normal'}`} > {_lnk.text} </span>
                                                }
                                            </a>
                                        </li>
                                    )
                            )
                        })}
                    </ul>
                </div>
            </div>
        </nav >
    )
}

export default SideNav;