import '../styles/globals.css';
import '../styles/sidebar.css';

import 'bootstrap/dist/css/bootstrap.min.css';

import '../public/vendor/owl-carousel/assets/owl.carousel.css';
import '../public/vendor/owl-carousel/assets/owl.theme.default.css';
// import '../styles/styled-carousel-dots.css';

import { ToastContainer } from 'react-toastify';
import initFirebase from './services/firebase';
import RemoteConfigsProvider from '../components/remote-config-provider';


//initialise firebase functions
initFirebase();
//END initialise firebase functions

function MyApp({ Component, pageProps }) {
  return (
    <RemoteConfigsProvider>
      <Component {...pageProps} />
      <ToastContainer />
    </RemoteConfigsProvider>
  )
}

export default MyApp
