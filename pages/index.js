import Headfile from '../components/head-file';
import dynamic from 'next/dynamic';
import Loader from 'react-loader';

import Card from 'react-bootstrap/Card'

import SideNav from '../components/sidebar';
import { useState } from 'react';

const OwlCarousel = dynamic(
  () => import('../components/owl-carousel'),
  { loading: () => <Loader loaded={false}></Loader>, ssr: false }
);

export default function Home() {

  const [activeLink, setActiveLink] = useState('link-1');

  return (
    <>
      <Headfile
        pageTitle={'Next Boilerplate'}
      />

      <SideNav
        theme={'dark'}
        position={'right'}
        activeColor={'#ffcc00'}
        activeLink={activeLink}
        bgColor={'#aaa'}
        onNavChange={(url) => { setActiveLink(url) }}
        links={[
          {
            text: 'Link 1',
            icon: "/img/map.svg",
            activeIcon: "/img/map-active.svg",
            url: '#',
            className: '',
            slug: 'link-1'
          },
          {
            text: 'Link 2',
            icon: "/img/inbox.svg",
            activeIcon: "/img/inbox-active.svg",
            url: '#',
            className: '',
            slug: 'link-2'
          },
          {
            text: 'Link 3',
            activeIcon: "/img/inbox-active.svg",
            url: '#',
            externalLink: true,
            className: '',
            slug: 'link-3'
          },
          {
            text: 'Link 4',
            icon: "/img/inbox.svg",
            activeIcon: "/img/inbox-active.svg",
            url: '#',
            className: '',
            slug: 'link-4',
            toBottom: true
          },
        ]}
      />
      <div className="row mx-0">

      </div>

      <div id="main">
        <div className="row mx-0 mt-5">
          <button className="btn btn-link text-decoration-none" id="custom-previous"> <img className="w-50" src="/img/left-icon.svg" /> </button>
          <div className="col-10 px-0 mx-auto">
            <style global jsx>
              {`
                /* Styling Pagination*/
                  .owl-theme .owl-dots .owl-dot span {
                    -webkit-border-radius: 0;
                    -moz-border-radius: 0;
                    border-radius: 0;
                    width: 100px;
                    height: 5px;
                    margin-left: 2px;
                    margin-right: 2px;
                    background: #ccc;
                    border: none;
                }

                .owl-theme .owl-dots .owl-dot.active span,
                .owl-theme .owl-dots.clickable .owl-dot:hover span {
                    background: #3F51B5;
                }
              `}
            </style>
            <OwlCarousel
              showDots={true}
              showNavs={true}
              customDots={'custom-dots'}
              customNavs={{
                previous: 'custom-previous',
                next: 'custom-next'
              }}
            >
              <Card className="mb-4 agentCard" >
                <Card.Img variant="top" src="../vercel.svg" className="w-100 p-3" />
                <Card.Body>
                  <div className="col-lg-8 col-12 my-auto pl-0 pr-0">
                    <h3 className="text-dark">{'agt.agent_name'}</h3>
                    <p className="text-muted mb-0">
                      <small>{'agt.agent_mobile'}</small>
                    </p>
                    <p className="text-muted mb-0">
                      <small>{'agt.agent_email'}</small>
                    </p>
                  </div>
                </Card.Body>
              </Card>

              <Card className="mb-4 agentCard" >
                <Card.Img variant="top" src="../vercel.svg" className="w-100 p-3" />
                <Card.Body>
                  <div className="col-lg-8 col-12 my-auto pl-0 pr-0">
                    <h3 className="text-dark">{'agt.agent_name'}</h3>
                    <p className="text-muted mb-0">
                      <small>{'agt.agent_mobile'}</small>
                    </p>
                    <p className="text-muted mb-0">
                      <small>{'agt.agent_email'}</small>
                    </p>
                  </div>
                </Card.Body>
              </Card>

              <Card className="mb-4 agentCard" >
                <Card.Img variant="top" src="../vercel.svg" className="w-100 p-3" />
                <Card.Body>
                  <div className="col-lg-8 col-12 my-auto pl-0 pr-0">
                    <h3 className="text-dark">{'agt.agent_name'}</h3>
                    <p className="text-muted mb-0">
                      <small>{'agt.agent_mobile'}</small>
                    </p>
                    <p className="text-muted mb-0">
                      <small>{'agt.agent_email'}</small>
                    </p>
                  </div>
                </Card.Body>
              </Card>

              <Card className="mb-4 agentCard" >
                <Card.Img variant="top" src="../vercel.svg" className="w-100 p-3" />
                <Card.Body>
                  <div className="col-lg-8 col-12 my-auto pl-0 pr-0">
                    <h3 className="text-dark">{'agt.agent_name'}</h3>
                    <p className="text-muted mb-0">
                      <small>{'agt.agent_mobile'}</small>
                    </p>
                    <p className="text-muted mb-0">
                      <small>{'agt.agent_email'}</small>
                    </p>
                  </div>
                </Card.Body>
              </Card>

              <Card className="mb-4 agentCard" >
                <Card.Img variant="top" src="../vercel.svg" className="w-100 p-3" />
                <Card.Body>
                  <div className="col-lg-8 col-12 my-auto pl-0 pr-0">
                    <h3 className="text-dark">{'agt.agent_name'}</h3>
                    <p className="text-muted mb-0">
                      <small>{'agt.agent_mobile'}</small>
                    </p>
                    <p className="text-muted mb-0">
                      <small>{'agt.agent_email'}</small>
                    </p>
                  </div>
                </Card.Body>
              </Card>

              <Card className="mb-4 agentCard" >
                <Card.Img variant="top" src="../vercel.svg" className="w-100 p-3" />
                <Card.Body>
                  <div className="col-lg-8 col-12 my-auto pl-0 pr-0">
                    <h3 className="text-dark">{'agt.agent_name'}</h3>
                    <p className="text-muted mb-0">
                      <small>{'agt.agent_mobile'}</small>
                    </p>
                    <p className="text-muted mb-0">
                      <small>{'agt.agent_email'}</small>
                    </p>
                  </div>
                </Card.Body>
              </Card>
            </OwlCarousel>
          </div>
          <button className="btn btn-link text-decoration-none" id="custom-next"> <img className="w-50" src="/img/right-icon.svg" /> </button>
        </div>
      </div>


    </>
  )
}
