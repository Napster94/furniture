import { Page, Text, Button, Row, useToast } from '@geist-ui/react';
import Router from 'next/router';
import { useEffect, useState, useContext } from 'react';
import firebase from 'firebase/app';
import 'firebase/auth';

import RemoteConfigContext from '../context/remote-config-context';

const provider = new firebase.auth.GoogleAuthProvider();
// provider.addScope('...');

export default function Register() {
    const { REMOTE_CONFIGS } = useContext(RemoteConfigContext);
    //TODO: always check for config key before using
    console.log(REMOTE_CONFIGS, REMOTE_CONFIGS.background_color && REMOTE_CONFIGS.background_color._value);

    const [authorizing, setAuthorizing] = useState(false);

    const _handleAuth = async () => {
        console.log('authorising...');
        setAuthorizing(true);

        try {
            const result = await firebase.auth().signInWithPopup(provider);
            const { user, credentials } = result;
            console.log({ user, credentials });

            if (!user) {
                throw new Error('There was an issue authorizing');
            }

            Router.push('/');
        } catch (error) {
            console.log(error);
        }

        setAuthorizing(false);
    }

    return (
        <Page style={{ background: '#fff' }}>
            <Row justify='center'>
                <Text h1>Register Now</Text>
            </Row>
            <Row justify='center'>
                <Text h2>Click to sign in or register</Text>
            </Row>
            <Row justify='center'>
                <Button loading={authorizing} onClick={_handleAuth}>
                    Get Started
                </Button>
            </Row>
        </Page>
    )
}